<?php

namespace backend\assets;

use yii\bootstrap4\BootstrapPluginAsset;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        'assets/vendor/nucleo/css/nucleo.css',
        'assets/vendor/@fortawesome/fontawesome-free/css/all.min.css',
        'assets/css/argon.css?v=1.2.0'
    ];
    public $js = [
        'assets/vendor/js-cookie/js.cookie.js',
        'assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js',
        'assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js',
        'assets/vendor/chart.js/dist/Chart.min.js',
        'assets/vendor/chart.js/dist/Chart.extension.js',
        'assets/js/argon.js?v=1.2.0'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        BootstrapPluginAsset::class
    ];
}
